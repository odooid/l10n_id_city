Indonesia State
===============



Bug Tracker
===========

Bugs are tracked on `GitLab Issues <https://gitlab.com/sendalpegat/odoo.id/oi_city/issues>`_.



Credits
=======

Contributors
------------
* aRai <arai@inspireforyou.com.com>

Maintainer
----------

.. image:: http://odoo.id/logo.png
   :alt: Odoo Indonesia - Konsultan Odoo Indonesia
   :target: http://inspireforyou.com
